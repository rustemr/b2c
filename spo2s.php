<meta charset="utf-8">

<?
ini_set("log_errors", 1);
// записываем ошибки возникающие при добавлении туров
ini_set("error_log", "spo_add.log");

/*
Описание ответов
1 - успешно записано 
2- запись уже существует
*/

// структурные массивы
$response = array(1 => 'Успешно записано', 'Запись уже существует'); 
$services = array('flights','countries','cities','hotels','buildings','airlines','airports','aircrafts','transfers','excursions','transports','boardings','currencies','tariffs','tourTypes', 'serviceDescriptions', 'rooms', 'roomCategories', 'categories','roomTypes', 'roomAccomodations', 'serviceClasses');
$table_fields = array('flight','country','city','hotel','building','airline','airport','aircraft','transfer','excursion','transport','boarding','currency','tariff','tourType','description','room','roomCategory','category', 'roomType','roomAccomodation', 'serviceClass');
 
// Объвление функций

// функция добавления типов экстра сервисов
function extra_description($id,$name_ru, $name_en, $class)
{
	$result=0;
	$count=check_coincidence($id,"extra_service_type");
	
	if ($count==0) {
		$query = mysql_query("
			INSERT INTO `extra_service_type` (`id`, `name_ru`, `name_en`,`class`) 
			VALUES ($id,'$name_ru','$name_en', '$class')
		");
		if (!$query) { $result = mysql_error(); }
		else { $result=1; }
    } 
	else { $result=2; }
	return $result;
}

// функция добавления полетов в таблицу spo_flights
function spo_flight($spo, $flight_id)
{    
	$result=0;
	$check = mysql_query("
		SELECT id FROM spo_flights WHERE spo_key='$spo' AND flight_key='$flight_id' LIMIT 1
	");
	// БЫСТРЫЙ запрос: время выполнения 0.0003 сек.
	$count=mysql_num_rows($check); 
	
	if ($count==0) {
		$insert_query = mysql_query("
			INSERT INTO `spo_flights` (`spo_key`, `flight_key`) 
			VALUES ('$spo','$flight_id')
		"); 
		 
		// проверям как прошла запись 
		if ($inset_query) { $result=1; }	
		else { $result=mysql_error(); }
	}
	else {
		$result=2;
	}
	return $result;
}

function insert_flights($spo_key, $flight_id, $departure_town_id,$arrival_town_id,$code,$departure_time,$arrival_time,$date_begin,$date_end,$week_days,$duration,$aircraft_id,$airlines_id,$arrival_airport_id,$departure_airport_id)
{
	//для начала проверим есть ли такой рейс в базе 
	$result=0;
	$query = mysql_query(" SELECT id FROM flights WHERE id='$flight_id' LIMIT 1 ");
	// БЫСТРЫЙ запрос: время выполнения 0.0003 сек.
	$count = mysql_num_rows($query); 
	
	if ($count==1) { 
		$result = spo_flight($spo_key, $flight_id);   
	}
	else{
		//рейса нет в базе 
		$query = mysql_query("
			INSERT INTO `flights`
			(`id`, `departure_town_id`, `arrival_town_id`, `code`, `departure_time`, `arrival_time`, `date_begin`, `date_end`, 
			`week_days`, `duration`, `aircraft_id`, `airlines_id`, `arrival_airport_id`, `departure_airport_id`) 
			VALUES ('$flight_id','$departure_town_id','$arrival_town_id','$code','$departure_time','$arrival_time','$date_begin','$date_end',
			'$week_days','$duration','$aircraft_id','$airlines_id','$arrival_airport_id','$departure_airport_id')
		"); 
		
		 if(!$query) { echo "Результаты инсерта ".$result=mysql_error();}
	     $result+=spo_flight($spo_key, $flight_id);	 
		}
	//
	return $result; 
}

function insert_to_dictionary_excurs($val,$table_name, $title, $title_lat,$code)
{
	$val=intval($val); 
	$query = mysql_query("INSERT INTO `".$table_name."`(`id`, `name_ru`, `name_en`,`city_id`) VALUES ('$val','$title','$title_lat','$code')");
	$result=0; 
	if (!$query) { $result=mysql_error(); } else { $result=1; }
    return $result;
}

function insert_to_dictionary_hotels($val,$table_name, $title, $title_lat,$country,$town)
{
	$val=intval($val); 
	$query = mysql_query("
		INSERT INTO `".$table_name."`(`id`, `name_ru`, `name_en`,`country_id`,`city_id`) 
		VALUES ('$val','$title','$title_lat','$country', '$town')
	");
	$result=0; 
	if (!$query) { $result = mysql_error(); } else { $result=1; }
    return $result;
}

function insert_to_dictionary_tariffs($val,$table_name, $title, $title_lat,$code,$spo_key)
{
	$val = intval($val); 
	$query = mysql_query("
		INSERT INTO `".$table_name."`(`id`, `name_ru`, `name_en`,`code`,`spo_key`) 
		VALUES ('$val','$title','$title_lat','$code', '$spo_key')
	");
	$result=0; 
	if (!$query) { $result=mysql_error(); } else { $result=1; }
    return $result;
}

function insert_to_dictionary($val,$table_name, $title, $title_lat)
{
	$val = intval($val); 
	$query = mysql_query("INSERT INTO `".$table_name."`(`id`, `name_ru`, `name_en`) VALUES ('$val','$title','$title_lat')");
	$result=0; 
	if (!$query) { $result=mysql_error(); } else { $result=1; }
    return $result;
}

function insert_to_dictionary_code($val,$table_name, $title, $title_lat,$code)
{
	$val=intval($val); 
	$query = mysql_query("
		INSERT INTO `".$table_name."`(`id`, `name_ru`, `name_en`,`code`) 
		VALUES ('$val','$title','$title_lat','$code')
	");
	$result=0; 
	if (!$query) { $result=mysql_error(); } else { $result=1; }
    return $result;
}

function insert_to_dictionary_city($val,$table_name, $title, $title_lat,$code)
{
	$val = intval($val); 
	$query = mysql_query("
		INSERT INTO `".$table_name."`(`id`, `name_ru`, `name_en`,`country_id`) 
		VALUES ('$val','$title','$title_lat','$code')
	");
	$result=0; 
	if (!$query) { $result=mysql_error(); } else { $result=1; }
    return $result;
}

function insert_to_dictionary_hotel_and_cat($val,$table_name, $title, $title_lat,$hotel, $cat)
{
	$val=intval($val); 
	$query = mysql_query("
		INSERT INTO `".$table_name."`(`id`, `name_ru`, `name_en`,`hotel_id`,`category_key`) 
		VALUES ('$val','$title','$title_lat','$hotel', '$cat')
	");
	$result=0; 
	if (!$query) { $result=mysql_error(); } else { $result=1; }
    return $result;
}

function insert_to_dictionary_transfers($val,$table_name, $title, $title_lat,$city)
{
	$val=intval($val); 
	$query=mysql_query("INSERT INTO `".$table_name."`(`id`, `name_ru`, `name_en`,`city_id`) VALUES ('$val','$title','$title_lat','$city')");
	$result=0; 
	if (!$query) { $result=mysql_error(); } else { $result=1; }
    return $result;
}

function insert_to_dictionary_code_city($val,$table_name, $title, $title_lat,$code, $city)
{
	$val=intval($val); 
	$query = mysql_query("
		INSERT INTO `".$table_name."`(`id`, `name_ru`, `name_en`,`code`,`city_key`) 
		VALUES ('$val','$title','$title_lat','$code','$city')
	");
	$result=0; 
	if (!$query) { $result=mysql_error(); } else { $result=1; }
    return $result;
}

function check_coincidence($val,$table_name)
{
	$val = intval($val); 
	$query = mysql_query("SELECT id FROM ".$table_name." WHERE id=".$val." LIMIT 1");
	// проверка на `service_set` - время исполнения 0.0003 сек
	// проверка на `spo_serviceset` - время исполнения 0.0003 сек
	// считаем, что запрос работает БЫСТРО
	
	if ($query) { $result=mysql_num_rows($query); }
	else { $result=mysql_error(); }
	return $result;	
}

function insert_to_spo($spo_key, $spo_val, $table_name, $table_field)
{
	$result=0;
	//проверем во избежание дубликатов
    $query = mysql_query("
		SELECT id FROM ".$table_name." WHERE spo_key='".$spo_key."' AND ".$table_field."_key='".$spo_val."' LIMIT 1
	");	
	// НЕПРОВЕРЕННЫЙ запрос: время выполнения ??????? сек.
	// проверено на spo_serviceset 0.0412 сек.
	
	echo $table_name."<br>";

	// если с запросом все норм 
	if ($query) {    
		// считаем колличество строк 
		$count=mysql_num_rows($query);
		
		// если запись новая 
		if ($count==0)
		{
			// то записываем в базу 
			$query = mysql_query(" INSERT INTO `".$table_name."` (`spo_key`, `".$table_field."_key`) VALUES ($spo_key,$spo_val) "); 
			if ($query) { $result=1; } else { $result=mysql_error();  }
		}
		else {
			//если запись уже существует
			$result=2;	
		}
	} 
	else {
		$result=mysql_error(); 
	}
	return $result;
}

function rooms($id, $type_id, $room_desc_id,$accomodation_id )
{
    $result=0;
	$query = mysql_query(" SELECT id FROM rooms WHERE id='$id' "); 
	// БЫСТРЫЙ запрос: время выполнения 0.0003 сек.
	
	$count = mysql_num_rows($query);
	if ($count==0)
	{
		$query = mysql_query("
			INSERT INTO `rooms`(`id`, `type_id`, `room_desc_id`, `accomodation_id`) 
			VALUES ($id,$type_id,$room_desc_id,$accomodation_id)
		");
		if(!$query) $result=mysql_error();
		else $result=1;	
	}
	else {
		$result=2;
	}
	return $result;
}

function room_categories($description_id, $name,$nameLat, $code)
{	
	$result=0;
	$query = mysql_query("SELECT description_id FROM room_descriptions WHERE description_id='$description_id' LIMIT 1");
	// БЫСТРЫЙ запрос: время выполнения 0.0003 сек.
	
	$count = mysql_num_rows($query);
	if ($count==0)
	{
		$query=mysql_query("INSERT INTO `room_descriptions`(`description_id`, `name`, `name_en`, `code`) VALUES ('$description_id', '$name','$nameLat', '$code')");
		if(!$query) $result=mysql_error();
		else $result=1;		
	}
	else {
		$result=2;
	}
	return $result;
}

function categories($key, $name, $nameLat, $spo_key)
{	
	$result=0;
	$query = mysql_query(" SELECT category_id FROM buldings_categories WHERE category_id='$key' LIMIT 1");
	// время исполнения запроса 0.0003 сек.
	
	$count = mysql_num_rows($query);
	if ($count==0)
	{
		$query = mysql_query("
			INSERT INTO `buldings_categories` (`category_id`, `spo_key`, `name`, `name_en`) 
			VALUES ('$key', '$spo_key', '$name', '$nameLat')
		");
		if(!$query) $result=mysql_error();
		else $result=1;
	}
	else {
		$result=2;
	}
	return $result;
}

function room_types($key, $name, $nameLat, $code, $numBed, $numExBed)
{
	$result=0;
	$query = mysql_query(" SELECT room_type_id FROM `room_types` WHERE room_type_id='$key' LIMIT 1"); 
	// БЫСТРЫЙ запрос: время выполнения 0.0003 сек.
	
	$count = mysql_num_rows($query);
	if ($count==0)
	{
		$query=mysql_query("
			INSERT INTO `room_types`(`room_type_id`, `name`, `name_en`, `code`, `numBed`, `numExBed`) 
			VALUES ('$key', '$name', '$nameLat', '$code','$numBed','$numExBed')
		");
		if(!$query) $result=mysql_error();
		else $result=1;
	}
	else {
		$result=2;
	}
	return $result;
}

function accomodation($key, $name, $nameLat, $numBed, $numExBed, $age_from, $age_to,$adult)
{	
	$result=0;
	$query = mysql_query("SELECT id FROM accomodations WHERE id='$key' LIMIT 1"); 
	// БЫСТРЫЙ запрос: время выполнения 0.0003 сек.
	
	$count = mysql_num_rows($query);
	if ($count==0)
	{
		$query=mysql_query("
			INSERT INTO `accomodations`(`id`, `name_ru`, `name_en`, `numBed`, `numExBed`, `age_from`, `age_to`, `adult`) 
			VALUES ($key, '$name', '$nameLat','$numBed','$numExBed','$age_from','$age_to','$adult')
		");
		if (!$query) $result=mysql_error();
		else $result=1;		
	}
	else {
		$result=2;
	}
	return $result;
}

function buildings($key, $name, $nameLat, $category, $hotel)
{	
	$result=0;
	$query = mysql_query("SELECT id FROM buildings WHERE id='$key' LIMIT 1");
	// БЫСТРЫЙ запрос: время выполнения 0.0003 сек.
	
	$count=mysql_num_rows($query);
	if($count==0)
	{
		$query = mysql_query("
			INSERT INTO `buildings`(`id`, `name_ru`, `name_en`, `hotel_id`, `category_key`) 
			VALUES ('$key','$name','$nameLat','$hotel','$category')
		");
		if(!$query) $result=mysql_error();
		else $result=1;		
	}
	else {
		$result=2;
	}
	return $result;
}

if (!function_exists('show_error')) {
	function show_error($error,$response) {
		$result=0;
		if(is_numeric($error)) {
			$result=$response[$error];	
		}
		else {
			$result=$error;
		}
		return $result;
	}
}

if (!function_exists('xml_attribute')) 
{
	function xml_attribute($object, $attribute) {
		if (isset($object[$attribute])) {
			return (string) $object[$attribute];
		}
	}
}

if (!function_exists('object2array')) 
{
	function object2array($object) { 
		return @json_decode(@json_encode($object),1); 
	} 
}

// подключение БД
require_once('mysql.php'); 

// обнуление рабочих переменных
$tt = 0;

//$query = mysql_query("SELECT * FROM spo_list WHERE spo_key='42040'");
$query_ever_new = mysql_query(" SELECT * FROM spo_list WHERE checked='0' ORDER BY RAND() LIMIT 100");
// время исполнения запроса 0.0007 сек.

while ($row=mysql_fetch_array($query_ever_new))
{
	$tt++;
	$spo_from_select = $row[spo_key];
	
	echo "<h1>$spo_from_select</h1>";
	$countries = array(); 
	
	//$request_url = "http://online.travelsystem.ru/TourML9/Service.asmx/GetSPOPageByKey?spokey=41271&minpricekey=100&pagesize=10&Login=exat&Password=HlfbHDHvJakLjYQDNP0jeHshf7o="; 
	//$request_url = "spo_25feb_41271.xml";
	//$request_url = "GetSPOPageByKey_2.xml";
	
	$request_url = "http://online.travelsystem.ru/TourML11/Service.asmx/GetSPOPageByKey?spokey=".$row['spo_key']."&minpricekey=100&pagesize=1000&Login=exat&Password=HlfbHDHvJakLjYQDNP0jeHshf7o="; 
	
	$time_start1 = microtime(true);
		$xml = simplexml_load_file($request_url);
	$time_end1 = microtime(true);
	$time1 = $time_end1-$time_start1;
	echo "<br><span style='color:red;'>Время получения XML: <b>$time1</b></span><br>";
	
	#######################################################################################
	##					Блок парсинга(подставляются назваие таблиц из массивов)	 		 ##																																
	#######################################################################################

	// проходимся циклом по массиву с названиями таблиц
	$country_kkey=0;
	foreach ($xml->references->countries->country as $crc) 
	{
		$country = $crc->attributes()->key;
		$country_name = $crc->attributes()->name;
		$countries[] = $country; 
	  
		if ($counntry!='460' && $country_name!='UNKNOWN') {
			$country_kkey=$country;
		}
	}
	
	// 	ИСКЛЮЧАЕМ комбинированные туры
	if (count($countries)!=3) {   
		unset($countries); 
		mysql_query("UPDATE `spo_list` SET `available`='0',`checked`='1' WHERE spo_key='$spo_from_select' LIMIT 1"); 
		continue;
	}
	
	// подключение дополнительных модулей для обработки
	$time_start2 = microtime(true);
		require('services.php');
	$time_end2 = microtime(true);	
	$time2 = $time_end2-$time_start2;
	echo "<br><span style='color:red;'>Время исполения файлов <b>services.php</b>: $time2</span><br>";
	
	$time_start2 = microtime(true);
		require('serviceset.php');
	$time_end2 = microtime(true);	
	$time2 = $time_end2-$time_start2;
	echo "<br><span style='color:red;'>Время исполения файлов <b>serviceset.php</b>: $time2</span><br>";
	
	$time_start2 = microtime(true);
		require('dates.php');
	$time_end2 = microtime(true);	
	$time2 = $time_end2-$time_start2;
	echo "<br><span style='color:red;'>Время исполения файлов <b>dates.php</b>: $time</span><br>";
	
	
	for ($i=0; $i<count($services); $i++)
	{
		foreach ($xml->references->$services[$i]->$table_fields[$i] as $crc) 
		{
			$city_iterations=0;
			
			//id страны проверяем что это не фейк + проверка на присутсвие страны в нашей базе
			if ($crc->attributes()->fake!="true")
			{
				if ($services[$i]=="roomAccomodations")
				{
					$key = $crc->attributes()->key;
					$name = $crc->attributes()->name;
					$nameLat = $crc->attributes()->nameLat;
					  
					if (isset($crc->attributes()->numExBed)) {
						$numExBed= $crc->attributes()->numExBed;
					}
					if (isset($crc->attributes()->numBed)) {
						$numExBed= $crc->attributes()->numBed;
					}
					
					if (isset($crc->child)) {
						$age_from=  $crc->child->attributes()->ageFrom;
						$age_to=  $crc->child->attributes()->ageTo;
						$adult=0;
					}
					else { $adult=1; }
					
					$result = accomodation($key, $name, $nameLat, $numBed, $numExBed, $age_from, $age_to,$adult);
					
					echo "<br>Результат добавления Accomodations :".show_error($result,$response)."<br>"; 
					continue;
				}
	  
				if ($services[$i]=="categories") {
					$key = $crc->attributes()->key;
					$name = $crc->attributes()->name;
					$nameLat = $crc->attributes()->nameLat;
					
					$result = categories($key, $name, $nameLat, $spo_from_select);
					echo "<br>Результат добавления Categories :".show_error($result,$response)."<br>"; 
					continue;
				}
		 
				if ($services[$i]=="roomTypes")
				{
					$key=$crc->attributes()->key;
					$name=$crc->attributes()->name;
					$nameLat=$crc->attributes()->nameLat;
					$code=$crc->attributes()->code;
					$numBed=$crc->attributes()->numBed;
					$numExBed=$crc->attributes()->numExBed;
					
					$result = room_types($key, $name, $nameLat, $code, $numBed, $numExBed);
					echo "<br>Результат добавления Room_types :".show_error($result,$response)."<br>"; 
					continue;
				}
		 
				if ($services[$i]=="buildings")
				{
					$key=$crc->attributes()->key;
					$name=$crc->attributes()->name;
					$nameLat=$crc->attributes()->nameLat;
					$category=$crc->attributes()->categoryKey;
					$hotel=$crc->attributes()->hotelKey;
			  
					$result = buildings($key, $name, $nameLat, $category, $hotel);
					echo "<br>Результат добавления Buildings :".show_error($result,$response)."<br>"; 
					continue;
				}
		
				if ($services[$i]=="roomCategories") 
				{
					$description_id=$crc->attributes()->key;
					$name=$crc->attributes()->name;
					$nameLat=$crc->attributes()->nameLat;
					$code=$crc->attributes()->code;
					$result=room_categories($description_id, $name,$nameLat, $code);
			
					echo "<br>Результат добавления категорий :".show_error($result,$response)."<br>"; 
					continue;
				}
		  
				if($services[$i]=="extraServices")
				{    
					$dayBeg=$crc->variant->attributes()->dayBeg;
					$nights=$crc->variant->attributes()->nights; 
					$days=$crc->variant->attributes()->days; 
					$men=$crc->variant->attributes()->men; 
					$service_id=$crc->variant->attributes()->id;
					$type_id= $men=$crc->attributes()->subKey;  
			  
					$result=extra_services($dayBeg, $nights, $days, $men, $sevice_id,$type_id);
					echo "<br>Результат добавления ExtraServices :".show_error($result,$response)."<br>"; 
					continue;
				}
	  
				if($services[$i]=="serviceDescriptions")
				{
					$id = $crc->attributes()->key;
					$title=$crc->attributes()->name;
					$class=$crc->attributes()->classKey;
					$result=extra_description($id, $title, $title, $class);
					echo "<br>Результат добавления serviceDescriptions :".show_error($result,$response)."<br>"; 
					continue;
				}
				
				if ($services[$i]=="serviceClasses")
				{
					// берем все необходимые данные по ...
					$key=$crc->attributes()->key;
					$name=$crc->attributes()->name;
					$name_en=$crc->attributes()->nameLat; 
			 
					$quert_check = mysql_query("SELECT key FROM serviceClasses WHERE key='$key' LIMIT 1"); 
					// БЫСТРЫЙ запрос: время выполнения 0.0003 сек.
	
					$count_check = mysql_num_rows($quert_check); 
					if ($count_check==0){
						$query=mysql_query("INSERT INTO `serviceClasses`(`key`, `name`, `nameLat`) VALUES ('$key','$name','$name_en')");
					}
					continue;
				}
	  
				if ($services[$i]=="flights")
				{
					$flight_id = $crc->attributes()->key;
					$departure_town_id=$crc->attributes()->fromCityKey;
					$arrival_town_id=$crc->attributes()->toCityKey;
					$code=$crc->attributes()->code;
					$departure_time=$crc->flightTime->attributes()->timeBegin;
					$arrival_time=$crc->flightTime->attributes()->timeEnd;
					$date_begin=$crc->flightTime->attributes()->dateBegin;
					$date_end=$crc->flightTime->attributes()->dateEnd;
					$week_days=$crc->flightTime->attributes()->weekDays;
					$duration=$crc->flightTime->attributes()->key;
					$aircraft_id=$crc->flightTime->attributes()->aircraftKey;
					$airlines_id=$crc->attributes()->airlineKey;
					$arrival_airport_id=$crc->flightTime->attributes()->fromAirportKey;
					$departure_airport_id=$crc->flightTime->attributes()->toAirportKey;
					
					$flights = insert_flights($spo_from_select, $flight_id, $departure_town_id,$arrival_town_id,$code,$departure_time,$arrival_time,$date_begin,$date_end,$week_days,$duration,$aircraft_id,$airlines_id,$arrival_airport_id,$departure_airport_id);
		  
					echo "<br><font color='#FF3300'>Результат добавления Flights: ".show_error($flights, $response)."</font><br>"; 
					unset ($flight_id, $departure_town_id,$arrival_town_id, $code, $departure_time,$arrival_time,$date_begin,$date_end,$week_days,$duration, $aircraft_id, $airlines_id, $arrival_airport_id,$departure_airport_id);
					continue;
				}
				
				//id страны 
				$country_key = $crc->attributes()->key;
				
				### проверка дала результат 0.0003 сек на едичные запросы
				$result = check_coincidence($country_key,$services[$i]); 
				
				//если нет в базе то добавляем 
				if ($result==0)
				{
					$country_title = $crc->attributes()->name;
					$country_title_Lat = $crc->attributes()->nameLat;
					$variant=0;
	
					if($services[$i]=='rooms')
					{
						$id= $crc->attributes()->key;
						$type_id= $crc->attributes()->typeKey;
						$room_desc_id= $crc->attributes()->roomDescKey;
						$accomodation_id =$crc->attributes()->accomodationKey;
		 
						$message = rooms($id, $type_id, $room_desc_id,$accomodation_id );
						echo "<br>Результат  добавления rooms: ".show_error($message, $response)."<br>";
						$variant=1;
					}
	
					if($services[$i]=='tourtypes')
					{
						$message = insert_to_dictionary($country_key,$country_title, $country_title);
						echo "<br>Результат добавления TourTypes :".show_error($message,$response)."<br>"; 
						$variant=1;
					}
					
					if($services[$i]=='transfers')
					{
						$code = $crc->attributes()->cityKey;
						$message = insert_to_dictionary_transfers($country_key,$services[$i],$country_title, $country_title,$code);
						echo "<br>Результат добавления Transfers :".show_error($message,$response)."<br>"; 
						$variant=1;
					}
					
					if ($services[$i]=='aircrafts' || $services[$i]=='airlines' || $services[$i]=='boardings' || $sevices[$i]=='currencies')
					{
						$code = $crc->attributes()->code;
						$message = insert_to_dictionary_code($country_key,$services[$i],$country_title, $country_title,$code);
						if ($sevices[$i]=='currencies') "ttttt".show_error($message,$response);
						echo "<br>Результат добавления $services[$i] :".show_error($message,$response)."<br>"; 
						$variant=1;
					}
					
					if($services[$i]=='cities')
					{ 
						$code = $crc->attributes()->countryKey;
						$message = insert_to_dictionary_city($country_key,$services[$i],$country_title, $country_title,$code);
						echo "<br>Результат добавления $services[$i] :".show_error($message,$response)."<br>"; 
						$variant=1;
		
						if ($city_iterations==0)
						{  
							$country_id=$code;
							$city_iterations=1;
						}
					}
					
					if($services[$i]=='excursions')
					{
						$code=$crc->attributes()->cityKey;
						$message=insert_to_dictionary_excurs($country_key,$services[$i],$country_title, $country_title,$code);
						echo "<br>Результат добавления $services[$i] :".show_error($message,$response)."<br>"; 
						$variant=1;
					}
					
					if($services[$i]=='hotels')
					{
						$city=$crc->attributes()->cityKey;
						$country=$crc->attributes()->countryKey;
						$message=insert_to_dictionary_hotels($country_key,$services[$i],$country_title, $country_title,$country, $city);
						echo "<br>Результат добавления $services[$i] :".show_error($message,$response)."<br>"; 
						$variant=1;
					}

					if($services[$i]=='airports')
					{
						$code=$crc->attributes()->code;
						$city_key=$crc->attributes()->cityKey;
						$message=insert_to_dictionary_code_city($country_key,$services[$i],$country_title, $country_title,$code,$city_key);
						echo "<br>Результат добавления $services[$i] :".show_error($message,$response)."<br>"; 
						$variant=1;
					}

					if ($services[$i]=='tariffs') {
						$code = $crc->attributes()->code;
						$message = insert_to_dictionary_tariffs($country_key,$services[$i],$country_title, $country_title,$code,$spo_from_select);
						echo "<br>Результат добавления $services[$i] :".show_error($message,$response)."<br>"; 
						$variant=1;
					}
	
					if ($variant==0) {
						$message=insert_to_dictionary($country_key,$services[$i],$country_title, $country_title_Lat);
					}
				}
			}
 
			$result = insert_to_spo($spo_from_select, $country_key, "spo_".$services[$i], $table_fields[$i]);
			if (is_numeric($result)) {
				//echo $response[$result]."<br>";
			}
			else {
				if (substr_count($result,"doesn't exist")==0) {
					echo "Ошибка при записи в SPO".$result."<br>";
				}
			}
		}
	}
	
	$query = mysql_query(" UPDATE `spo_list` SET `checked`='1' WHERE spo_key='$spo_from_select' LIMIT 1 ");
	################################    Конец блока парсинга   ################################ 
	
	
	// добавление нескольких вариантов c одной SPOPage на витрину
	$query = mysql_query("
		SELECT b.tour_id, a.price AS price, a.date_key
		FROM spo_serviceset a, spo_list b
		WHERE a.spo_key ='$spo_from_select'	AND a.spo_key = b.spo_key
		LIMIT 1
	");
	// время исполнения запроса 0.0843 сек
	
	while ($virt_row=mysql_fetch_array($query))
	{
		//запись в витрину
		$insert_query = mysql_query("
			INSERT INTO `vitrina`(`tour_id`, `country`, `date_key`, `published`) 
			VALUES ('$virt_row[tour_id]','$country_kkey','$virt_row[date_key]','1')
		");
		
		echo $insert_query;
		if(!$insert_query) echo mysql_error();
	}

	//sleep(5);
	unset($city_iterations,$country_id,$country_kkey);
}

echo "num: ".$tt;
?>